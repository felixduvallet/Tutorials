/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    StateChartTutorial::ExampleStateChartGroup
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveJoints.h"

using namespace armarx;
using namespace ExampleStateChartGroup;

// DO NOT EDIT NEXT LINE
MoveJoints::SubClassRegistry MoveJoints::Registry(MoveJoints::GetName(), &MoveJoints::CreateInstance);



MoveJoints::MoveJoints(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<MoveJoints>(stateData),  MoveJointsGeneratedBase<MoveJoints>(stateData)
{
}

void MoveJoints::onEnter()
{
    // get the target joint values
    std::map<std::string, float> jointValueMap = in.getJointTargetPose();

    //build conditions for OnPoseReached
    Term poseReachedConditions;
    const float eps = 0.05f;
    for (const auto & jointNameValue : jointValueMap)
    {
        std::string jointNameDatafield = "Armar3KinematicUnitObserver.jointangles." + jointNameValue.first;
        float jointValue = jointNameValue.second;

        Literal jointValueReached(jointNameDatafield, "inrange",
                                  Literal::createParameterList(jointValue - eps, jointValue + eps));
        poseReachedConditions = poseReachedConditions && jointValueReached;
    }

    installConditionForOnPoseReached(poseReachedConditions);
}

void MoveJoints::run()
{
    std::map<std::string, float> jointVelocityMap = in.getJointVelocity();
    NameControlModeMap velocityControlModeMap;
    for (const auto & jointVelocity : jointVelocityMap)
    {
        velocityControlModeMap[jointVelocity.first] = eVelocityControl;
    }
    KinematicUnitInterfacePrx kinUnit = getKinematicUnit();
    kinUnit->switchControlMode(velocityControlModeMap);
    kinUnit->setJointVelocities(jointVelocityMap);

}

void MoveJoints::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void MoveJoints::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveJoints::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveJoints(stateData));
}

