/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MotionPlanningTutorials
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#include <RobotAPI/libraries/core/Pose.h>

#include <RobotComponents/components/MotionPlanning/CSpace/SimoxCSpace.h>
#include <RobotComponents/components/MotionPlanning/CSpace/ScaledCSpace.h>

#include "SimpleAStarPathSearchExample.h"


///REMOVE
#include <Inventor/nodes/SoSeparator.h> //for sosep
#include <exception>
#include <algorithm>
#include <cmath>
#include <unordered_map>

#include <RobotAPI/libraries/core/Pose.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>




void armarx::SimpleAStarPathSearchExampleComponent::onInitComponent()
{


    pServerName = getProperty<std::string>("PlanningServerName").getValue();
    wMemName = getProperty<std::string>("WorkingMemoryName").getValue();

    usingProxy(pServerName);
    usingProxy(wMemName);
}

void armarx::SimpleAStarPathSearchExampleComponent::onConnectComponent()
{

    wMemProxy = getProxy<memoryx::WorkingMemoryInterfacePrx>(wMemName);
    pServerProxy = getProxy<MotionPlanningServerInterfacePrx>(pServerName);

    if(!wMemProxy)
    {
        ARMARX_ERROR_S << "no working memory";
        return;
    }
    if(!pServerProxy)
    {
        ARMARX_ERROR_S << "no motion planning server";
        return;
    }

    if(rrtHandle)
    {
        //only start one task
        return;
    }

    memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx = wMemProxy->getPriorKnowledge()->getObjectClassesSegment();
    memoryx::CommonStorageInterfacePrx cStorageProxy = wMemProxy->getCommonStorage();

    if(!classesSegmentPrx)
    {
        ARMARX_ERROR_S << "no classes segment";
        return;
    }
    if(!cStorageProxy)
    {
        ARMARX_ERROR_S << "common storage";
        return;
    }

    ARMARX_INFO_S << "now setting up the cspace";
    //set up cspace



    ///REMOVE

//    SimoxCSpacePtr cspace = new SimoxCSpace(cStorageProxy);
    SimoxCSpaceWith2DPosePtr cspace = new SimoxCSpaceWith2DPose(cStorageProxy);

    //agent
    {
        AgentPlanningInformation agentData;

        //the agents position in the space
        agentData.agentPose = new Pose{Eigen::Matrix3f::Identity(), Eigen::Vector3f {-250, -1200, -50}};
        //define information required to load the agent
        agentData.agentProjectName = "RobotAPI";
        agentData.agentRelativeFilePath = "RobotAPI/robots/Armar3/ArmarIII.xml";
        //define chains for kollision checking and movement planning
        agentData.collisionSetNames.emplace_back("PlatformTorsoHeadColModel");
        agentData.collisionSetNames.emplace_back("RightArmHandColModel");

        //we don't want to plan a kinematic chain
        // => leave agentData.kinemaicChainNames empty

        //attach object
        AttachedObject attached;
        //the object is in the right tcp (you can use any node. you could "glue" something to armars back)
        attached.attachedToRobotNodeName="TCP R";
        //and its collision model is associated with the right arm's collision model
        // => no collision check is performed between the object and the right arm
        attached.associatedCollisionSetName="RightArmHandColModel";
        //the object is a bottle
        memoryx::EntityBasePtr classesEntity = classesSegmentPrx->getEntityByName("bottle");
        attached.objectClassBase = memoryx::ObjectClassBasePtr::dynamicCast(classesEntity);
        //and its pose in relation to the right tcp has to be set
        //you may want to use an actual grasp here
        attached.relativeObjectPose = new Pose{};
        agentData.attachedObjects.emplace_back(attached);

        //you can set initial values for each joint (they will not be changed during planning (except they are for joints used during planning))
        agentData.initialJointValues["Elbow L"] = -1.57;
        agentData.initialJointValues["Shoulder 1 L"] = -1.36;

        //the agent is set for usage
        cspace->setAgent(agentData);
    }


    //add a table to the scene
    {
        //load
        std::string objClassName {"table"};
        memoryx::EntityBasePtr classesEntity = classesSegmentPrx->getEntityByName(objClassName);

        //you could use this code to check for errors when loading the object
//        if (!classesEntity)
//        {
//            ARMARX_ERROR_S << "No memory entity found with name " << objClassName;
//            return;
//        }

        memoryx::ObjectClassBasePtr objectClassBase = memoryx::ObjectClassBasePtr::dynamicCast(classesEntity);

        //you could use this code to check for errors when loading the object
//        if (!objectClassBase)
//        {
//            ARMARX_ERROR_S << "Could not cast entitiy to object class, name: " << objClassName;
//            return;
//        }

        //add
        StationaryObject table;
        //set the table as object
        table.objectClassBase = objectClassBase;
        //the table is at its default pose
        table.objectPose =  new Pose{};
        //add it to the cspace
        cspace->addStationaryObject(table);
    }

    Vector3fRange poseBounds;
    poseBounds.min.x = -5000;
    poseBounds.min.y = -5000;
    poseBounds.min.z = -M_PI;
    poseBounds.max.x =  5000;
    poseBounds.max.y =  5000;
    poseBounds.max.z =  M_PI;
    cspace->setPoseBounds(poseBounds);

    ARMARX_INFO_S << "now setting up the task";
    //set up task (use here your start and end configuration)
    //               x     y  rz
    VectorXf start{-250,-1200,-M_PI_2};
    VectorXf goal {-250, 1200, M_PI_2};

    //since we are planning translation and rotation at the same time we should scale the translation axis down
    //(if we don't: 1mm translation == 1rad rotation)
    ScaledCSpacePtr scaledCspace = new ScaledCSpace{cspace, {0.01, 0.01, 1}};
    scaledCspace->scaleConfig(start);
    scaledCspace->scaleConfig(goal);

    //create the task
    MotionPlanningTaskBasePtr task = new RRTConnectTask{scaledCspace, start, goal};

    ARMARX_INFO_S << "now running the task";
    //run task
    rrtHandle = pServerProxy->enqueueTask(task);
}

