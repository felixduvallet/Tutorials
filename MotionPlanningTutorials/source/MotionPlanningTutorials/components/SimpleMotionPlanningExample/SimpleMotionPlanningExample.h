/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MotionPlanningTutorials
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_COMPONENT_MotionPlanningTutorials_SimpleMotionPlanningExampleComponent_H
#define _ARMARX_COMPONENT_MotionPlanningTutorials_SimpleMotionPlanningExampleComponent_H

#include <ArmarXCore/core/Component.h>
#include <MemoryX/interface/component/WorkingMemoryInterface.h>

#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandle.h>
//??/libraries/RemoteHandle/ClientSideRemoteHandleControlBlock.h>

#include <RobotComponents/interface/components/MotionPlanning/MotionPlanningServer.h>
#include <RobotComponents/components/MotionPlanning/Tasks/RRTConnect/Task.h>

namespace armarx
{
    class SimpleMotionPlanningExampleComponent;

    typedef IceInternal::Handle<SimpleMotionPlanningExampleComponent> ExamplePlanningComponentPtr;

    class SimpleMotionPlanningExampleComponentPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        SimpleMotionPlanningExampleComponentPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("PlanningServerName", "MotionPlanningServer", "The planning server's name.");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "The used working memory. (for objects)");
        }
    };

    class SimpleMotionPlanningExampleComponent:
        virtual public Component
    {
    public:
        /**
         * @brief ctor
         */
            SimpleMotionPlanningExampleComponent()=default;
        /**
         * @brief dtor
         */
        virtual ~SimpleMotionPlanningExampleComponent()=default;

        // inherited from Component
        virtual std::string getDefaultName() const override
        {
            return "SimpleMotionPlanningExampleComponent";
        }
        virtual void onInitComponent() override;
        virtual void onConnectComponent() override;
        virtual void onDisconnectComponent() override {}
        virtual void onExitComponent() override {}

        virtual PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(new SimpleMotionPlanningExampleComponentPropertyDefinitions(getConfigIdentifier()));
        }

        protected:
            std::string pServerName;
            std::string wMemName;

            memoryx::WorkingMemoryInterfacePrx wMemProxy;
            MotionPlanningServerInterfacePrx pServerProxy;
            RRTConnectTaskHandle rrtHandle;
    };
}
#endif
