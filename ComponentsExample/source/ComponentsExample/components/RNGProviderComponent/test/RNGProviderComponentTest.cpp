/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ComponentsExample::ArmarXObjects::RNGProviderComponent
 * @author     Michael Bechtel ( michael dot bechtel at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ComponentsExample::ArmarXObjects::RNGProviderComponent

#define ARMARX_BOOST_TEST

#include <ComponentsExample/Test.h>
#include <ComponentsExample/components/RNGProviderComponent/RNGProviderComponent.h>
#include <ComponentsExample/components/RNGCallerComponent/RNGCallerComponent.h>

#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include <iostream>

BOOST_AUTO_TEST_CASE(testExample)
{
    armarx::RNGProviderComponent instance;

    BOOST_CHECK_EQUAL(true, true);
}

BOOST_AUTO_TEST_CASE(testExample2)
{
    armarx::RNGCallerComponent instance;

    BOOST_CHECK_EQUAL(true, true);
}


