#include <path/to/your/component.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>


using namespace armarx;

class RNGComponentTestEnvironment
{
public:
    RNGComponentTestEnvironment(const std::string& testName, int registryPort = 11220, bool addObjects = true)
    {
        Ice::PropertiesPtr properties = Ice::createProperties();
        armarx::Application::LoadDefaultConfig(properties);
        
        _iceTestHelper = new IceTestHelper(registryPort, registryPort+1);
        _iceTestHelper->startEnvironment();
        
        _manager = new TestArmarXManager (testName, _iceTestHelper->getCommunicator(), properties);
        
        if (addObjects)
        {
            _yourComponent = _manager->createComponentAndRun<RNGProviderComponent,RNGProviderComponentInterfacePrx>("ArmarX", "RNGProviderComponent");
        }
    }
    
    ~RNGComponentTestEnvironment()
    {
        _manager->shutdown();
    }
    
    RNGProviderComponentInterfacePrx _yourComponent;
    TestArmarXManagerPtr _manager;
    IceTestHelperPtr _iceTestHelper;   
};

typedef boost::shared_ptr<YourComponentTestEnvironment> YourComponentTestEnvironmentPtr;
