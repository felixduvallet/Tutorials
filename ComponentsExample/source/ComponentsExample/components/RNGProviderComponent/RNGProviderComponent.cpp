/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ComponentsExample::ArmarXObjects::RNGProviderComponent
 * @author     Michael Bechtel ( michael dot bechtel at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RNGProviderComponent.h"


using namespace armarx;


Ice::Int RNGProviderComponent::generateRandomInt(const Ice::Current &c)
{
    return gen();
}

void RNGProviderComponent::onInitComponent()
{
    std::time_t now = std::time(0);
    gen = boost::random::mt19937{static_cast<std::uint32_t>(now)};
}


void RNGProviderComponent::onConnectComponent()
{
    ARMARX_IMPORTANT << "RNG output: " << generateRandomInt();
}


void RNGProviderComponent::onDisconnectComponent()
{

}


void RNGProviderComponent::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr RNGProviderComponent::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new RNGProviderComponentPropertyDefinitions(
                                      getConfigIdentifier()));
}

