#ifndef ARMARX_RNGPROVIDERCOMPONENT_SLICE
#define ARMARX_RNGPROVIDERCOMPONENT_SLICE
module armarx
{
    interface RNGProviderComponentInterface
    {
        int generateRandomInt();
    };
};
#endif
