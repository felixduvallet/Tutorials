# Add your components below as shown in the following example:
#
set(SCENARIO_COMPONENTS
	CommonStorage
	PriorKnowledge
	LongtermMemory
	WorkingMemory
)


# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario("MemoryX" "${SCENARIO_COMPONENTS}")

