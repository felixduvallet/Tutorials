/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MultiThreadingExample::application::MultiThreadingExampleServerApp
 * @author     Michael Bechtel ( michael dot bechtel at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_APPLICATION_MultiThreadingExample_MultiThreadingExampleServerApp_H
#define _ARMARX_APPLICATION_MultiThreadingExample_MultiThreadingExampleServerApp_H


// #include <MultiThreadingExample/components/@MyComponent@/@MyComponent@.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>

#include <MultiThreadingExample/components/MultiThreadingExampleServerComponent/MultiThreadingExampleServerComponent.h>

namespace armarx
{
    /**
     * @class MultiThreadingExampleServerAppApp
     * @brief A brief description
     *
     * Detailed Description
     */
    class MultiThreadingExampleServerAppApp :
        virtual public armarx::Application
    {
        /**
         * @see armarx::Application::setup()
         */
        void setup(const armarx::ManagedIceObjectRegistryInterfacePtr& registry,
                   Ice::PropertiesPtr properties)
        {
            registry->addObject( armarx::Component::create<MultiThreadingExampleServerComponent>(properties) );
        }
    };
}

#endif
