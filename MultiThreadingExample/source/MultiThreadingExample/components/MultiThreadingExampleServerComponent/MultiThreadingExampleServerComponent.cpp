/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MultiThreadingExample::ArmarXObjects::MultiThreadingExampleServerComponent
 * @author     Michael Bechtel ( michael dot bechtel at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MultiThreadingExampleServerComponent.h"


using namespace armarx;


void MultiThreadingExampleServerComponent::setValue(const std::string &value, const Ice::Current &)
{
    this->inputValue = value;
}

std::string MultiThreadingExampleServerComponent::getValue(const Ice::Current &)
{
    return this->inputValue;
}

void MultiThreadingExampleServerComponent::onInitComponent()
{

}


void MultiThreadingExampleServerComponent::onConnectComponent()
{
    serverTask->start();

}


void MultiThreadingExampleServerComponent::onDisconnectComponent()
{
    serverTask->stop();
}


void MultiThreadingExampleServerComponent::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr MultiThreadingExampleServerComponent::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new MultiThreadingExampleServerComponentPropertyDefinitions(
                                              getConfigIdentifier()));
}

void MultiThreadingExampleServerComponent::runningTask()
{
    while(!serverTask->isStopped())
    {
        std::string tmp;
        {
            boost::mutex::scoped_lock lock(inputMutex);
            tmp = inputValue; //keep lock time short
        }
        sleep(2); //sleep simulates long computation time
        boost::hash<std::string> string_hash;
        ARMARX_IMPORTANT << "The hash value of \"" + inputValue + "\" is " + ValueToString(string_hash(inputValue));
        {
            boost::mutex::scoped_lock lock(outputMutex);
            outputValue = tmp; //keep lock time short
        }
    }
}

