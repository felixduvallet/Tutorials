/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MultiThreadingExample::ArmarXObjects::MultiThreadingExampleServerComponent
 * @author     Michael Bechtel ( michael dot bechtel at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_MultiThreadingExample_MultiThreadingExampleServerComponent_H
#define _ARMARX_COMPONENT_MultiThreadingExample_MultiThreadingExampleServerComponent_H


#include <ArmarXCore/core/Component.h>

#include <MultiThreadingExample/interface/MultiThreadingExampleServerInterface.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

namespace armarx
{
    /**
     * @class MultiThreadingExampleServerComponentPropertyDefinitions
     * @brief
     */
    class MultiThreadingExampleServerComponentPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        MultiThreadingExampleServerComponentPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-MultiThreadingExampleServerComponent MultiThreadingExampleServerComponent
     * @ingroup MultiThreadingExample-Components
     * A description of the component MultiThreadingExampleServerComponent.
     * 
     * @class MultiThreadingExampleServerComponent
     * @ingroup Component-MultiThreadingExampleServerComponent
     * @brief Brief description of class MultiThreadingExampleServerComponent.
     * 
     * Detailed description of class MultiThreadingExampleServerComponent.
     */
    class MultiThreadingExampleServerComponent :
        virtual public armarx::Component,
            virtual public armarx::MultiThreadingExampleServerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "MultiThreadingExampleServerComponent";
        }

        virtual void setValue(const std::string &value, const Ice::Current &);
        virtual std::string getValue(const Ice::Current &);

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

    private:
        virtual void runningTask();

        RunningTask<MultiThreadingExampleServerComponent>::pointer_type serverTask = new RunningTask<MultiThreadingExampleServerComponent>(this, &MultiThreadingExampleServerComponent::runningTask);
        boost::mutex inputMutex, outputMutex;

        std::string inputValue, outputValue;

        // MultiThreadingExampleServerInterface interface;

    };
}

#endif
