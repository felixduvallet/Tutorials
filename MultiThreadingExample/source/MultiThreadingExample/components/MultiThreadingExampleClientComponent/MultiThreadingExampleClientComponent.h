/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MultiThreadingExample::ArmarXObjects::MultiThreadingExampleClientComponent
 * @author     Michael Bechtel ( michael dot bechtel at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_MultiThreadingExample_MultiThreadingExampleClientComponent_H
#define _ARMARX_COMPONENT_MultiThreadingExample_MultiThreadingExampleClientComponent_H


#include <ArmarXCore/core/Component.h>
#include <MultiThreadingExample/interface/MultiThreadingExampleServerInterface.h>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>


namespace armarx
{
    /**
     * @class MultiThreadingExampleClientComponentPropertyDefinitions
     * @brief
     */
    class MultiThreadingExampleClientComponentPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        MultiThreadingExampleClientComponentPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("ClientType", "Sender or Receiver");
            defineOptionalProperty<std::string>("ServerName", "MultiThreadingExampleServer", "Description");
        }
    };

    /**
     * @defgroup Component-MultiThreadingExampleClientComponent MultiThreadingExampleClientComponent
     * @ingroup MultiThreadingExample-Components
     * A description of the component MultiThreadingExampleClientComponent.
     * 
     * @class MultiThreadingExampleClientComponent
     * @ingroup Component-MultiThreadingExampleClientComponent
     * @brief Brief description of class MultiThreadingExampleClientComponent.
     * 
     * Detailed description of class MultiThreadingExampleClientComponent.
     */
    class MultiThreadingExampleClientComponent :
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "MultiThreadingExampleClientComponent";
        }


    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();
    private:
        bool isSender;
        int counter = 0;
        MultiThreadingExampleServerInterfacePrx serverPrx;
        PeriodicTask<MultiThreadingExampleClientComponent>::pointer_type clientTask;

        virtual void periodicTaskCallback();




    };
}

#endif
