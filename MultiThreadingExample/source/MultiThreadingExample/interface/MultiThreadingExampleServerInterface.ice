#ifndef _ARMARX_MultiThreadingExample_ServerInterface_SLICE
#define _ARMARX_MultiThreadingExample_ServerInterface_SLICE
module armarx
{
    interface MultiThreadingExampleServerInterface
    {
        void setValue(string val);
        string getValue();
    };
};
#endif
