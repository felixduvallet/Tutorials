#define BOOST_TEST_MODULE ArmarX::Tutorials::PickAndPlaceTutorialScenarioTestExample
#define ARMARX_BOOST_TEST

#include <PickAndPlaceTutorial/Test.h>

// Includes the environment for testing whole statechart-scenarios with access to memory components
#include <MemoryX/core/test/StatechartScenarioMemoryTestEnv.h>

using namespace armarx;
using namespace memoryx;

BOOST_AUTO_TEST_CASE(PickAndPlaceTutorialScenarioTestExample)
{
    // Initialising the testing-environment, but this time with access to memory components.
    // It is possible to set the path to a specific database, that should be used for this test.
    StatechartScenarioMemoryTestEnvironment env("PickAndPlaceTutorialScenarioTestExample", "/ArmarXDB/data/ArmarXDB/dbexport/memdb");
    env.startScenario("ArmarXSimulation", "Armar3Simulation", "Simulator", 5000);
    env.startScenario("PickAndPlaceTutorial", "PickAndPlaceScenario");

    env.watcher->waitForStateChartFinished(90000);

    // The output-parameters of the statechart are not used in this example, but they get accessed like this
    StringVariantContainerBaseMap statechartOutput = env.watcher->getStateChartOutput();


    // Trying to check the position of the objectInstance "vitaliscereal" after the statechart has finished correctly
    try
    {
        ObjectInstanceMemorySegmentBasePrx objectInstances = env.memoryAccess->workingMemory->getObjectInstancesSegment();
        ObjectInstancePtr vitaliscereal = ObjectInstancePtr::dynamicCast(objectInstances->getObjectInstanceByName("vitaliscereal"));

        BOOST_CHECK_EQUAL(vitaliscereal, !false);

        float eps = 1.0f; // percent

        int expectedXPosition = 2089;
        int expectedYPosition = 5221;
        int expectedZPosition = 788;

        BOOST_CHECK_CLOSE(vitaliscereal->getPosition()->x, expectedXPosition, eps);
        BOOST_CHECK_CLOSE(vitaliscereal->getPosition()->y, expectedYPosition, eps);
        BOOST_CHECK_CLOSE(vitaliscereal->getPosition()->z, expectedZPosition, eps);

    }
    catch (...)
    {
        handleExceptions();
    }
}
