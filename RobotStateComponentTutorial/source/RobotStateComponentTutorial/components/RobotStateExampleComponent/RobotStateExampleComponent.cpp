/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotStateComponentTutorial::ArmarXObjects::RobotStateExampleComponent
 * @author     Martin Do ( martin dot do at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotStateExampleComponent.h"


using namespace armarx;


void RobotStateExampleComponent::onInitComponent()
{
    usingTopic("RobotState");
    offeringTopic("DebugDrawerUpdates");
    cycleTime = 50;
     if (execTask) execTask->stop();
        execTask = new PeriodicTask<RobotStateExampleComponent>(this, &RobotStateExampleComponent::periodicExec, cycleTime, false, "RobotStateExampleComponent");
    execTask->setDelayWarningTolerance(300);
}


void RobotStateExampleComponent::onConnectComponent()
{
    ScopedLock lock(accessMutex);

    drawer = getTopic<DebugDrawerInterfacePrx>("DebugDrawerUpdates");

    robotStateComponentPrx = getProxy<RobotStateComponentInterfacePrx>("RobotStateComponent");

    localRobot = RemoteRobot::createLocalClone(robotStateComponentPrx);

    selectedRobotNodeName = "";
    Eigen::Vector3f newTargetVec(0,0,0);
    targetPositionGlobal = new FramedPosition(newTargetVec, GlobalFrame, "");
    bStarted = true;

    bShowCoordinateSystem = false;
    bShowTargetVector = false;
    execTask->start();
}


void RobotStateExampleComponent::onDisconnectComponent()
{
    ScopedLock lock(accessMutex);
    drawer->clearLayer("testLayer");
    bStarted = false;

    execTask->stop();
}


void RobotStateExampleComponent::onExitComponent()
{

}

void RobotStateExampleComponent::showRobotNodePoseCoordinateSystem(std::string robotNodeName, bool bShow)
{
    RemoteRobot::synchronizeLocalClone(localRobot,robotStateComponentPrx);


    if (robotNodeName.compare("") == 0)
        return;
    if (localRobot->getRobotNode(robotNodeName))
    {
        Eigen::Matrix4f robotNodePose = Eigen::Matrix4f::Identity();
        FramedPosePtr robotNodeFramedPose = new FramedPose(robotNodePose, robotNodeName, localRobot->getRootNode()->getName());
        robotNodeFramedPose->changeToGlobal(localRobot);

        if (bShow)
            drawer->setPoseVisu("testLayer", robotNodeName, robotNodeFramedPose);
        else
            drawer->removePoseVisu("testLayer", robotNodeName);
    }

}

void RobotStateExampleComponent::showTargetPosition(FramedPositionPtr targetPosition, bool bShow)
{
    FramedPositionPtr newTargetPosition =  new FramedPosition(targetPosition->toEigen(),targetPosition->getFrame(),targetPosition->agent);

    if (bShow)
        drawer->setSphereVisu("testLayer", "TargetPosition",  (Vector3BasePtr)newTargetPosition, armarx::DrawColor{1, 0, 0, 1},200);
    else
        drawer->removeSphereVisu("testLayer", "TargetPosition");
}

void RobotStateExampleComponent::showTargetVector(std::string robotNodeName, FramedPositionPtr targetPosition, bool bShow)
{
    RemoteRobot::synchronizeLocalClone(localRobot,robotStateComponentPrx);
    if (robotNodeName.compare("") == 0)
        return;
    if (localRobot->getRobotNode(robotNodeName))
    {
        FramedPositionPtr newTargetPosition =  new FramedPosition(targetPosition->toEigen(),targetPosition->getFrame(),targetPosition->agent);
        newTargetPosition->changeFrame(localRobot,robotNodeName);
        FramedPositionPtr targetPositionLocal = newTargetPosition;

        Eigen::Vector3f robotNodePosition;
        FramedPositionPtr robotNodeFramedPosition = new FramedPosition(robotNodePosition, robotNodeName, localRobot->getRootNode()->getName());
        robotNodeFramedPosition->changeToGlobal(localRobot);

        FramedDirectionPtr targetDirection = new FramedDirection(targetPositionLocal->toEigen(),robotNodeName,localRobot->getRootNode()->getName());
        targetDirection->changeToGlobal(localRobot);
        if (bShow)
            drawer->setArrowVisu("testLayer", robotNodeName, robotNodeFramedPosition,targetDirection, armarx::DrawColor{0, 0, 1, 1},targetDirection->toEigen().norm(),20);
        else
            drawer->removeArrowVisu("testLayer", robotNodeName);
    }

}

void RobotStateExampleComponent::periodicExec()
{
    showRobotNodePoseCoordinateSystem(selectedRobotNodeName, bShowCoordinateSystem);
    showTargetVector(selectedRobotNodeName, targetPositionGlobal, bShowTargetVector);
    showTargetPosition(targetPositionGlobal, bShowTargetPosition);

    //else
    //    drawer->re
}

void RobotStateExampleComponent::setSelectedRobotNodeName(const std::string& rnName, const Ice::Current& c)
{
    showRobotNodePoseCoordinateSystem(selectedRobotNodeName, false);
    showTargetVector(selectedRobotNodeName, targetPositionGlobal, false);
    selectedRobotNodeName = rnName;
}

void RobotStateExampleComponent::setTargetPosition(const Vector3BasePtr& newTarget, const Ice::Current& c)
{
    Eigen::Vector3f newTargetVec(newTarget->x,newTarget->y,newTarget->z);
    targetPositionGlobal = new FramedPosition(newTargetVec, GlobalFrame, "");
}

void RobotStateExampleComponent::setShowCoordinateSystem(bool bShowCoordSys, const Ice::Current& c)
{
    bShowCoordinateSystem = bShowCoordSys;
}

void RobotStateExampleComponent::setShowTargetVector(bool bShowTargetVec, const Ice::Current& c)
{
    bShowTargetVector = bShowTargetVec;
}

void RobotStateExampleComponent::setShowTargetPosition(bool bShowTargetPos, const Ice::Current& c)
{
    bShowTargetPosition = bShowTargetPos;
}

std::vector<std::string> RobotStateExampleComponent::getRobotNodeNames(const Ice::Current& c)
{
    std::vector<std::string> robotNodeNames;
    std::vector<VirtualRobot::RobotNodePtr> robotNodes;
    localRobot->getRobotNodes(robotNodes);
    for (int i = 0; i < robotNodes.size(); i++)
        robotNodeNames.push_back(robotNodes[i]->getName());
    return robotNodeNames;
}

armarx::PropertyDefinitionsPtr RobotStateExampleComponent::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new RobotStateExampleComponentPropertyDefinitions(
                                      getConfigIdentifier()));
}

