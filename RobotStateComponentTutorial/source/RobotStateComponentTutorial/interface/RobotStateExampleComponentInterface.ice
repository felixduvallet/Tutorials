#ifndef ROBOTSTATEEXAMPLECOMPONENT_INTERFACE_SLICE
#define ROBOTSTATEEXAMPLECOMPONENT_INTERFACE_SLICE

#include <RobotAPI/interface/core/RobotState.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{

    interface RobotStateExampleComponentInterface
    {
        void setSelectedRobotNodeName(string rnName);
        void setTargetPosition(Vector3Base newTarget);
        void setShowCoordinateSystem(bool bShowCoordSys);
        void setShowTargetVector(bool bShowTargetVec);
        void setShowTargetPosition(bool bShowTargetPos);

        StringSequence getRobotNodeNames();
    };

};

#endif
