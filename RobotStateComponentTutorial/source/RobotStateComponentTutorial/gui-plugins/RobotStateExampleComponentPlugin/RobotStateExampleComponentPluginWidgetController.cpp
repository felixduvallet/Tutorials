/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotStateComponentTutorial::gui-plugins::RobotStateExampleComponentPluginWidgetController
 * @author     Martin Do ( martin dot do at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotStateExampleComponentPluginWidgetController.h"

#include <string>

using namespace armarx;

RobotStateExampleComponentPluginWidgetController::RobotStateExampleComponentPluginWidgetController()
{
    widget.setupUi(getWidget());


    connect(widget.robotNodeListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(selectRobotNodeName()));
    connect(widget.showCoordSysCheckBox, SIGNAL(stateChanged(int)), this, SLOT(setShowCoordinateSystem()));
    connect(widget.showTargetPosCheckBox, SIGNAL(stateChanged(int)), this, SLOT(setShowTargetPosition()));
    connect(widget.showTargetVecCheckBox, SIGNAL(stateChanged(int)), SLOT(setShowTargetVector()));
    connect(widget.targetXLineEdit, SIGNAL(editingFinished()), this, SLOT(update()));
    connect(widget.targetYLineEdit, SIGNAL(editingFinished()), this, SLOT(update()));
    connect(widget.targetZLineEdit, SIGNAL(editingFinished()), this, SLOT(update()));
    rsExCompProxyName = std::string("RobotStateExampleComponent");
}


RobotStateExampleComponentPluginWidgetController::~RobotStateExampleComponentPluginWidgetController()
{

}


void RobotStateExampleComponentPluginWidgetController::loadSettings(QSettings* settings)
{

}

void RobotStateExampleComponentPluginWidgetController::saveSettings(QSettings* settings)
{

}


void RobotStateExampleComponentPluginWidgetController::onInitComponent()
{

}


void RobotStateExampleComponentPluginWidgetController::onConnectComponent()
{
    rsExCompPrx = getProxy<RobotStateExampleComponentInterfacePrx>(rsExCompProxyName);
    std::vector<std::string> rnsNames = rsExCompPrx->getRobotNodeNames();
    for (int i = 0; i < rnsNames.size(); i++)
        widget.robotNodeListWidget->addItem(QString(rnsNames[i].c_str()));
    widget.displayedRobotNodeNameLineEdit->setText(QString(rnsNames[0].c_str()));
    rsExCompPrx->setSelectedRobotNodeName(rnsNames[0]);
    widget.targetXLineEdit->setText(QString("0"));
    widget.targetYLineEdit->setText(QString("0"));
    widget.targetZLineEdit->setText(QString("0"));
    update();
}

void RobotStateExampleComponentPluginWidgetController::selectRobotNodeName()
{
    QList<QListWidgetItem*> selectedRobotNodeItem = widget.robotNodeListWidget->selectedItems();
    std::string selecteRobotNodeName = selectedRobotNodeItem[0]->text().toStdString();
    rsExCompPrx->setSelectedRobotNodeName(selecteRobotNodeName);
    widget.displayedRobotNodeNameLineEdit->setText(selectedRobotNodeItem[0]->text());
}

void RobotStateExampleComponentPluginWidgetController::setShowCoordinateSystem()
{
    rsExCompPrx->setShowCoordinateSystem(widget.showCoordSysCheckBox->isChecked());
}

void RobotStateExampleComponentPluginWidgetController::setShowTargetPosition()
{
    rsExCompPrx->setShowTargetPosition(widget.showTargetPosCheckBox->isChecked());
}

void RobotStateExampleComponentPluginWidgetController::setShowTargetVector()
{
    rsExCompPrx->setShowTargetVector(widget.showTargetVecCheckBox->isChecked());
}

void RobotStateExampleComponentPluginWidgetController::update()
{
    float xCoord = widget.targetXLineEdit->text().toFloat();
    float yCoord = widget.targetYLineEdit->text().toFloat();
    float zCoord = widget.targetZLineEdit->text().toFloat();
    Eigen::Vector3f newTargetVec(xCoord,yCoord,zCoord);
    FramedPositionPtr targetPosition = new FramedPosition(newTargetVec, GlobalFrame, "");

    rsExCompPrx->setTargetPosition(targetPosition);
}
