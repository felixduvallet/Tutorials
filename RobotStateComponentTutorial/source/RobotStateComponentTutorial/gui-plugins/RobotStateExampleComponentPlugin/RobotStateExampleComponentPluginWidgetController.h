/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotStateComponentTutorial::gui-plugins::RobotStateExampleComponentPluginWidgetController
 * @author     Martin Do ( martin dot do at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_RobotStateComponentTutorial_RobotStateExampleComponentPlugin_WidgetController_H
#define _ARMARX_RobotStateComponentTutorial_RobotStateExampleComponentPlugin_WidgetController_H

#include "ui_RobotStateExampleComponentPluginWidget.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <RobotStateComponentTutorial/interface/RobotStateExampleComponentInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx
{
    /**
     * @class RobotStateExampleComponentPluginWidgetController
     * @ingroup ArmarXGuiPlugins
     * @brief RobotStateExampleComponentPluginWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        RobotStateExampleComponentPluginWidgetController:
    public armarx::ArmarXComponentWidgetController
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit RobotStateExampleComponentPluginWidgetController();

        /**
         * Controller destructor
         */
        virtual ~RobotStateExampleComponentPluginWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        virtual void loadSettings(QSettings* settings);

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        virtual void saveSettings(QSettings* settings);

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        virtual QString getWidgetName() const
        {
            return "RobotStateExampleComponentPlugin";
        }

        /**
         * @see armarx::Component::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::Component::onConnectComponent()
         */
        virtual void onConnectComponent();

    public slots:
        /* QT slot declarations */

        void selectRobotNodeName();
        void setShowCoordinateSystem();
        void setShowTargetPosition();
        void setShowTargetVector();
        void update();

    signals:
        /* QT signal declarations */

    private:
        /**
         * Widget Form
         */
        Ui::RobotStateExampleComponentPluginWidget widget;
        RobotStateExampleComponentInterfacePrx  rsExCompPrx;
        std::string  rsExCompProxyName;
    };
}

#endif
